# C# Developer Logic Assessment
#### Ve Interactive

### Instructions
Please write a program or API that can generate a one-time password and verify if the password is valid for one user only. 

The input of the program should be: 

* User ID to generate the password

* Use the User ID and the password to verify the validity of the password.

* Every generated password should be valid for 30 seconds.

You are free to use a Web, MVC, Console or Class Library project in order to accomplish the requirement.

### Evaluation criteria

Your solution will be evaluated based on coding standards, naming conventions, project 

structure and meeting the requirements. Note: the use of unit testing is highly recommended. 

### Submitting Test

Please submit your answer/solution to us in a ZIP file by email or send us a link to the solution 

in GitHub. Do not include compiled objects or third party DLLs if you are sending the solution via email.

-------------------------

## Implementation details

Implementations has been extracted from https://en.wikipedia.org/wiki/Time-based_One-time_Password_Algorithm

A time based one time password seemed a fit solution for the task at hand.

The 30 seconds validity for the algorithm is achieved by generating the same password per user within windows of 30 seconds.

As specified in https://tools.ietf.org/html/rfc6238#page-6


