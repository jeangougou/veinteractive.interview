﻿namespace veInteractive.Common
{
  public interface IProgramFlow
  {
    void Main(string[] args, IOneTimePassword otp);
  }
}