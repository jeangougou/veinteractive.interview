﻿using System;

namespace veInteractive.Common
{
  public interface IOneTimePassword
  {
    /// <summary>
    /// generate a one time password
    /// </summary>
    /// <param name="input">user id input</param>
    /// <param name="dateTimeSeed">datetime reference</param>
    /// <returns></returns>
    string Generate(string input, DateTime dateTimeSeed);

    /// <summary>
    /// Validate a one time password
    /// </summary>
    /// <param name="input">user id input</param>
    /// <param name="oneTimePassword">password to compare</param>
    /// <param name="dateTimeSeed">datetime reference to re-generate the comparison value</param>
    /// <returns></returns>
    bool IsValid(string input, string oneTimePassword, DateTime dateTimeSeed);

    /// <summary>
    /// returns the reference datetime for the current One Time Password
    /// </summary>
    /// <returns>date time for the generator</returns>
    DateTime GetSeedDateTime();
  }
}