﻿using System;
using System.Security.Cryptography;
using System.Text;
using veInteractive.Common;

namespace veInteractive
{
  public class OneTimePassword : IOneTimePassword
  {
    private readonly DateTime dateTimeEpoch = DateTime.UtcNow.AddDays(-1);
    private dynamic config = new Formo.Configuration();

    /// <summary>
    /// generates password from input and datetime seed
    /// </summary>
    /// <param name="input">expected userid</param>
    /// <param name="dateTimeSeed">datetime seed</param>
    /// <returns></returns>
    public string Generate(string input, DateTime dateTimeSeed)
    {
      var serverSecret = config.ServerSecret<string>();
      var derivedInput = string.Format("{0}{1}", input, serverSecret);
      // server secret prevents plain timing attacks
      byte[] derivedInputByte = Encoding.ASCII.GetBytes(derivedInput);

      var userIdHmac = new HMACSHA256(derivedInputByte);
      var offsetFromEpoch = GetIterationByDateSeed(dateTimeSeed);
      var iterationBytes = BitConverter.GetBytes(offsetFromEpoch);
      return HexStringFromBytes(userIdHmac.ComputeHash(iterationBytes)); 
    }

    public bool IsValid(string input, string oneTimePassword, DateTime dateTimeSeed)
    {
      return Generate(input, dateTimeSeed) == oneTimePassword;
    }

    public DateTime GetSeedDateTime()
    {
      return DateTime.UtcNow;
    }

    public string HexStringFromBytes(byte[] bytes)
    {
      var sb = new StringBuilder();
      foreach (byte b in bytes)
      {
        var hex = b.ToString("x2");
        sb.Append(hex);
      }
      return sb.ToString();
    }

    /// <summary>
    /// defines string based on time
    /// </summary>
    /// <param name="dateTimeSeed"></param>
    /// <returns></returns>
    private long GetIterationByDateSeed(DateTime dateTimeSeed)
    {
        // ensure it's a utc date format
        var dtsUtc = DateTime.SpecifyKind(dateTimeSeed, DateTimeKind.Utc);
        return (long)(dtsUtc - dateTimeEpoch).TotalSeconds / this.TimeIntervalInSeconds;
    }

    /// <summary>
    /// read from configuration
    /// </summary>
    private int TimeIntervalInSeconds
    {
      get
      {
        return config.TimeIntervalInSeconds<int>(30);
      }
    }
  }
}