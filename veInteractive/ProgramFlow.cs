﻿using System;
using System.Linq;
using veInteractive.Common;

namespace veInteractive
{
  public enum AllowedOperation
  {
    Generate = 0,
    Validate = 1
  }

  public class ProgramFlow : IProgramFlow
  {
    private IOneTimePassword otp;

    public void Main(string[] args, IOneTimePassword otp)
    {
      // prints usage
      Usage();
      // early exit on disallowed commands
      AllowedOperation selectedOperation;
      if (!Enum.TryParse<AllowedOperation>(args.FirstOrDefault(), out selectedOperation))
      {
        Usage("invalid command");
      }

      if (AllowedOperation.Generate == selectedOperation)
      {
        // generate requires an input, hence the second parameter
        if (args.Length != 2)
        {
          Usage(string.Format("invalid number of commands, please add only your userId after the {0} option", AllowedOperation.Generate));
        }
        var generatedOtp = otp.Generate(
          input: args[1],
          dateTimeSeed: otp.GetSeedDateTime());
        Console.WriteLine("your current password is: {0}", generatedOtp);
        return;
      }

      if (AllowedOperation.Validate == selectedOperation)
      {
        // validation requires an input and the old password
        if (args.Length != 3)
        {
          Usage(string.Format("invalid number of commands, please add only your userId after the {0} option", AllowedOperation.Generate));
        }

        var mes = "you password is invalid";
        if (otp.IsValid(
              input: args[1],
              oneTimePassword: args[2],
              dateTimeSeed: otp.GetSeedDateTime()))
        {
          mes = "you password is Valid";
        }
        Console.WriteLine(mes);
        return;
      }
    }

    /// <summary>
    /// prints application usage
    /// </summary>
    /// <param name="errorMessage">throws argument exception with the specified message</param>
    private static void Usage(string errorMessage = "")
    {
      Console.WriteLine("type or specify {0} followed by the enter key to generate a new password based on your userId", AllowedOperation.Generate);
      Console.WriteLine("type or specify {0} followed by the enter key to validate your userId and password", AllowedOperation.Validate);
      Console.WriteLine("your password will be valid only for 30 seconds");

      if (!string.IsNullOrEmpty(errorMessage))
      {
        throw new ArgumentException(errorMessage);
      }
    }
  }
}