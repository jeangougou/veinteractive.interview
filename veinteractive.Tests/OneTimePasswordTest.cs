﻿using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using veInteractive;
using veInteractive.Common;

namespace veinteractive.Tests
{
  [TestClass]
  public class OneTimePasswordTest
  {
    private int TimeIntervalInSeconds
    {
      get
      {
        dynamic config = new Formo.Configuration();
        return config.TimeIntervalInSeconds<int>(1);
      }
    }

    private ProgramFlow mockFlow;
    private IOneTimePassword mockOtp;
    private IOneTimePassword concreteOtp;

    [TestInitialize]
    public void Setup()
    {
      mockOtp = A.Fake<IOneTimePassword>();
      mockFlow = new veInteractive.ProgramFlow();
      concreteOtp = new OneTimePassword();
    }

    #region argument numers

    [TestMethod]
    public void ThrowWhenIncorrectParametersOnGenerate()
    {
      try
      {
        var program = new ProgramFlow();
        program.Main(
          new string[] {
            veInteractive.AllowedOperation.Generate.ToString(),
            Faker.Name.FullName()
          },
          concreteOtp);
      }
      catch (ArgumentException ex)
      {
        Assert.IsTrue(ex.Message.Contains(AllowedOperation.Generate.ToString()));
      }
      catch (Exception ex)
      {
        Assert.Fail();
      }
    }

    #endregion argument numers

    [TestMethod]
    public void GenerateCommandWillCallGenerationMethod()
    {
      var input = Faker.Name.FullName();
      // call to generate a new otp
      mockFlow.Main(
        new string[] {
          veInteractive.AllowedOperation.Generate.ToString(),
          input
        },
        mockOtp);
      A.CallTo(() => mockOtp.Generate(input, mockOtp.GetSeedDateTime())).MustHaveHappened();
    }

    [TestMethod]
    public void ValidateCommandWillCallValidationMethod()
    {
      var input = Faker.Name.FullName();
      var initialDate = concreteOtp.GetSeedDateTime();
      var initialOtp = concreteOtp.Generate(input, initialDate);
      // call to validate otp
      mockFlow.Main(
        new string[] {
          veInteractive.AllowedOperation.Validate.ToString(),
          input,
          initialOtp
        },
        mockOtp);
      A.CallTo(() => mockOtp.IsValid(input, initialOtp, mockOtp.GetSeedDateTime())).MustHaveHappened();
    }

    [TestMethod]
    public void DifferentUsersGenerateDifferentOneTimePasswords()
    {
      var input1 = Faker.Name.FullName();
      var input2 = Faker.Name.FullName();
      var initialDate = concreteOtp.GetSeedDateTime();
      var input1Otp = concreteOtp.Generate(input1, initialDate);
      var input2Otp = concreteOtp.Generate(input2, initialDate);
      input1.Should().NotBe(input2);
      input1Otp.Should().NotBe(input2Otp);
    }

    [TestMethod]
    public void PasswordChangesEveryTimeInterval()
    {
      var input = Faker.Name.FullName();
      var initialDate = concreteOtp.GetSeedDateTime();
      var afterIntervalDate = initialDate.AddSeconds(TimeIntervalInSeconds);
      var initialOtp = concreteOtp.Generate(input, initialDate);
      var afterIntervalOtp = concreteOtp.Generate(input, afterIntervalDate);
      afterIntervalOtp.Should().NotBe(initialOtp);
    }

    [TestMethod]
    public void PasswordCanBeValid()
    {
      var input = Faker.Name.FullName();
      var dateTimeMark = concreteOtp.GetSeedDateTime();
      var initialOtp = concreteOtp.Generate(input, dateTimeMark);
      concreteOtp.IsValid(input, initialOtp, dateTimeMark).Should().Be(true);
    }

    [TestMethod]
    public void PasswordShouldExpireAfterTimeInterval()
    {
      var input = Faker.Name.FullName();
      var initialDate = DateTime.UtcNow.AddSeconds(-DateTime.UtcNow.Second);
      var afterIntervalDate = initialDate.AddSeconds(TimeIntervalInSeconds+1);
      var initialOtp = concreteOtp.Generate(input, initialDate);
      concreteOtp.IsValid(input, initialOtp, afterIntervalDate).Should().Be(false);
    }

    [TestMethod]
    public void PasswordShouldNotExpireWithinTimeIntervalFromAnyTime()
    {
      var input = Faker.Name.FullName();
      var initialDate = DateTime.UtcNow;
      var afterIntervalDate = initialDate.AddSeconds(TimeIntervalInSeconds - 1);
      var initialOtp = concreteOtp.Generate(input, initialDate);
      concreteOtp.IsValid(input, initialOtp, afterIntervalDate).Should().Be(true);
    }

    [TestMethod]
    public void PasswordGeneratedWithinTimeIntervalIsSameFromAnyTime()
    {
        var input = Faker.Name.FullName();
        var initialDate = DateTime.UtcNow;
        var afterIntervalDate = initialDate.AddSeconds(TimeIntervalInSeconds - 1);
        var initialOtp = concreteOtp.Generate(input, initialDate);
        var afterIntervalOtp = concreteOtp.Generate(input, afterIntervalDate);
        afterIntervalOtp.Should().Be(initialOtp);
    }

    [TestMethod]
    public void PasswordGeneratedAfterTimeIntervalIsDifferetFromRoundedTime()
    {
      var input = Faker.Name.FullName();
      var initialDate = DateTime.UtcNow.AddSeconds(-DateTime.UtcNow.Second);
      var afterIntervalDate = initialDate.AddSeconds(TimeIntervalInSeconds + 1);
      var initialOtp = concreteOtp.Generate(input, initialDate);
      var afterIntervalOtp = concreteOtp.Generate(input, afterIntervalDate);
      afterIntervalOtp.Should().NotBe(initialOtp);
    }
  }
}